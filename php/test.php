<?php



if (!isset($_SESSION))
{
    session_start();
    $_SESSION["user_id"]="1";



}
header('Content-Type: text/html; charset=utf-8');


include 'db.php';


if ($_SERVER["REQUEST_METHOD"] == "POST")
{
    $dbase = new db();

    if ($_POST['query']==='getCourse')
    {
        $query = "SELECT CourseNumber FROM CourseList";
        $course =[];

       // $course = array("CSE 1101","CSE 1102","CSE 1103","CSE 1104","CSE 1105","Others");
        $result = $dbase->query($query);
        while($row=mysqli_fetch_row($result))
        {
            $course[]=$row[0];

        }
        echo json_encode($course);
    }

    else if ($_POST['query']==='getRooms')
    {
       // $rooms = array("307","320","420","413");
        $query = "SELECT RoomNumber FROM RoomList";
        $rooms =[];

        // $course = array("CSE 1101","CSE 1102","CSE 1103","CSE 1104","CSE 1105","Others");
        $result = $dbase->query($query);
        while($row=mysqli_fetch_row($result))
        {
            $rooms[]=$row[0];

        }
        echo json_encode($rooms);
    }


    //// for lab

    else if ($_POST['query']==='getLabs')
    {
       // $rooms = array("307","320","420","413");
        $query = "SELECT LabNumber FROM LabList";
        $rooms =[];

        // $course = array("CSE 1101","CSE 1102","CSE 1103","CSE 1104","CSE 1105","Others");
        $result = $dbase->query($query);
        while($row=mysqli_fetch_row($result))
        {
            $rooms[]=$row[0];

        }
        echo json_encode($rooms);
    }


    else if ($_POST['query']==='getTimes')
    {
       // $times = array("08:30am - 10:00am","10:00am - 11:30am","11:30am - 01:00pm","02:00pm - 03:30pm","03:30pm - 05:00pm");

        $query = "SELECT TimeRange FROM TimeSlot";
        $times =[];

        // $course = array("CSE 1101","CSE 1102","CSE 1103","CSE 1104","CSE 1105","Others");
        $result = $dbase->query($query);
        while($row=mysqli_fetch_row($result))
        {
            $times[]=$row[0];

        }

        echo json_encode($times);
    }

    else if ($_POST['query']==='saveTimeRange')
    {
        $okay = $_POST['data'];
        $okay2 = $_POST['Course'];
        $okay3 = $_POST['Comment'];
        $_SESSION["timeData"]=$okay;
        $_SESSION["Course"]=$okay2;
        $_SESSION["Comment"]=$okay3;
        echo "success";
    }


    else if ($_POST['query']==='saveTimeRangeLab')
    {
        $okay = $_POST['data'];
        $okay2 = $_POST['Course'];
        $okay3 = $_POST['Comment'];
        $_SESSION["timeDataLab"]=$okay;
        $_SESSION["CourseLab"]=$okay2;
        $_SESSION["CommentLab"]=$okay3;
        echo "success";
    }




    else if ($_POST['query']==='getAvailableSlots') // queried from classroommultipleday.html
    {

        $arrayOfQuery = $_SESSION["timeData"];
        $obj = json_decode($arrayOfQuery);
        $myRoom = $obj[0]->Room;
        $myTime = $obj[0]->Time;
        //$myCourse=$obj[0]->Course;
        $query2= "SELECT Position from TimeSlot where TimeRange= '".$myTime."'";
        $result1 = $dbase->query($query2);
        if($row=mysqli_fetch_row($result1))
        {
            $position = $row[0];
            $_SESSION["Position"]=$position;
            $id = 0;
            $tishpish = [];
            foreach ($obj as $key => $val)
            {
                $room = $val->Room;
                $time = $val->Time;
                $date = $val->Date;
                $query = "SELECT * FROM ClassBookingList WHERE RequestedDate = '" . $date . "' and RoomNumber = '" . $room . "' and Position = '" . $position . "'";
                $result2 = $dbase->query($query);

                if ($bookingID = mysqli_fetch_row($result2)) 
                {
                    // if this returns any row then the slot is anyhow occupied.. not free :v 
                    // LOGIC :v
                }
                else
                {
                    $tishpish[]=json_encode($val);
                }
                
            }
            echo json_encode($tishpish);

        }
        else
        {
            //Position error;
            echo "Failed";
        }

    }



    else if ($_POST['query']==='getAvailableSlotsLab') // queried from labroommultipleday.html
    {

        $arrayOfQuery = $_SESSION["timeDataLab"];
        $obj = json_decode($arrayOfQuery);
        $myRoom = $obj[0]->Room;
        $myTime = $obj[0]->Time;
        //$myCourse=$obj[0]->Course;
        $query2= "SELECT Position from TimeSlot where TimeRange= '".$myTime."'";
        $result1 = $dbase->query($query2);
        if($row=mysqli_fetch_row($result1))
        {
            $position = $row[0];
            $_SESSION["Position"]=$position;
            $id = 0;
            $tishpish = [];
            foreach ($obj as $key => $val)
            {
                $room = $val->Room;
                $time = $val->Time;
                $date = $val->Date;
                $query = "SELECT * FROM LabBookingList WHERE RequestedDate = '" . $date . "' and RoomNumber = '" . $room . "' and Position = '" . $position . "'";
                $result2 = $dbase->query($query);

                if ($bookingID = mysqli_fetch_row($result2)) 
                {
                    // if this returns any row then the slot is anyhow occupied.. not free :v 
                    // LOGIC :v
                }
                else
                {
                    $tishpish[]=json_encode($val);
                }
                
            }
            echo json_encode($tishpish);

        }
        else
        {
            //Position error;
            echo "Failed";
        }

    }




    else if ($_POST['query']==='bookSelectedSlots')
    {

        $date      = date("m-d-y");
        $status="pending";

        $arrayOfQuery = $_POST['data'];
        $objarr = json_decode($arrayOfQuery);

        foreach($objarr as $obj)
        {
            $room = $obj->Room;

            $pos =$obj->Position;
            $Date = $obj->Date;
            $course=$obj->Course;
            $comment = $obj->Comemnt;
            $User_id = $_SESSION["user_id"];

            $query= "INSERT INTO ClassBookingList
                (UserID,RoomNumber,Position,RequestedDate,Date,CourseNumber,Comment,Status) 
                VALUES ('$User_id','$room','$pos','$Date','$date','$course',
                '$comment','$status')";

                $result = $dbase->query($query);
                if($result)
                {
                    echo "success";
                }
                else
                {
                    echo mysql_error ($result);
                }

        }
    }

    else if ($_POST['query']==='bookSelectedSlotsLab')
    {

        $date      = date("m-d-y");
        $status="pending";

        $arrayOfQuery = $_POST['data'];
        $objarr = json_decode($arrayOfQuery);

        foreach($objarr as $obj)
        {
            $room = $obj->Room;

            $pos =$obj->Position;
            $Date = $obj->Date;
            $course=$obj->Course;
            $comment = $obj->Comemnt;
            $User_id = $_SESSION["user_id"];

            $query= "INSERT INTO LabBookingList
                (UserID,RoomNumber,Position,RequestedDate,Date,CourseNumber,Comment,Status) 
                VALUES ('$User_id','$room','$pos','$Date','$date','$course',
                '$comment','$status')";

                $result = $dbase->query($query);
                if($result)
                {
                    echo "success";
                }
                else
                {
                    echo mysql_error ($result);
                }

        }
    }








    else if ($_POST['query']==='bookMultipleDays')
    {

        $date      = date("m-d-y");
        $status="taken";

        $arrayOfQuery = $_POST['data'];
        $objarr = json_decode($arrayOfQuery);

        foreach($objarr as $obj)
        {
            
            $objj = json_decode($obj);
            $room = $objj->Room;
            $pos = $_SESSION["Position"];
            $Date = $objj->Date;
            $course=$_SESSION["Course"];
            $comment = $_SESSION["Comment"];
            $User_id = $_SESSION["user_id"];

            $query= "INSERT INTO ClassBookingList
                (UserID,RoomNumber,Position,RequestedDate,Date,CourseNumber,Comment,Status) 
                VALUES ('$User_id','$room','$pos','$Date','$date','$course',
                '$comment','$status')";

                $result = $dbase->query($query);
                if($result)
                {
                    echo "success";
                }
                else
                {
                    echo mysql_error ($result);
                }

        }
        
    }



    else if ($_POST['query']==='bookMultipleDaysLab')
    {

        $date      = date("m-d-y");
        $status="taken";

        $arrayOfQuery = $_POST['data'];
        $objarr = json_decode($arrayOfQuery);

        foreach($objarr as $obj)
        {
            
            $objj = json_decode($obj);
            $room = $objj->Room;
            $pos = $_SESSION["Position"];
            $Date = $objj->Date;
            $course=$_SESSION["CourseLab"];
            $comment = $_SESSION["CommentLab"];
            $User_id = $_SESSION["user_id"];

            $query= "INSERT INTO LabBookingList
                (UserID,RoomNumber,Position,RequestedDate,Date,CourseNumber,Comment,Status) 
                VALUES ('$User_id','$room','$pos','$Date','$date','$course',
                '$comment','$status')";

                $result = $dbase->query($query);
                if($result)
                {
                    echo "success";
                }
                else
                {
                    echo mysql_error ($result);
                }

        }
        
    }



    if($_POST['query']==='getBookedSlots')
    {
        //echo "hello:";
        $date = $_POST['date'];

        $query="SELECT POSITION, RoomNumber,CourseNumber,Status FROM ClassBookingList WHERE RequestedDate = '".$date."'";

        $result = $dbase->query($query);
        for ($set = array (); $row = $result->fetch_assoc(); $set[] = $row);
        //print_r($set);
        echo json_encode($set);


    }

    if($_POST['query']==='getBookedSlotsLab')
    {
        //echo "hello:";
        $date = $_POST['date'];

        $query="SELECT POSITION, RoomNumber,CourseNumber,Status FROM LabBookingList WHERE RequestedDate = '".$date."'";

        $result = $dbase->query($query);
        for ($set = array (); $row = $result->fetch_assoc(); $set[] = $row);
        //print_r($set);
        echo json_encode($set);

    }

}

else
{
    echo "ERROR in QUERY";
}



?>
