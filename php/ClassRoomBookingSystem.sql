-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Oct 01, 2017 at 08:44 AM
-- Server version: 10.1.22-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ClassRoomBookingSystem`
--

-- --------------------------------------------------------

--
-- Table structure for table `ClassBookingList`
--

CREATE TABLE `ClassBookingList` (
  `BookingID` bigint(20) NOT NULL,
  `UserID` varchar(2000) DEFAULT NULL,
  `CourseNumber` varchar(100) NOT NULL,
  `RoomNumber` varchar(100) NOT NULL,
  `Position` int(255) NOT NULL,
  `RequestedDate` varchar(100) NOT NULL,
  `Date` varchar(100) NOT NULL,
  `Time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `Status` varchar(100) NOT NULL,
  `Comment` varchar(2000) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ClassBookingList`
--

INSERT INTO `ClassBookingList` (`BookingID`, `UserID`, `CourseNumber`, `RoomNumber`, `Position`, `RequestedDate`, `Date`, `Time`, `Status`, `Comment`) VALUES
(40, '1', 'CSE 1104', '420', 0, '10/01/2017', '10-01-17', '2017-09-30 19:18:56', 'taken', 'hello 1'),
(41, '1', 'CSE 1105', '420', 3, '10/07/2017', '10-01-17', '2017-09-30 19:19:32', 'taken', 'tishpish'),
(42, '1', 'CSE 1105', '420', 3, '10/14/2017', '10-01-17', '2017-09-30 19:19:32', 'taken', 'tishpish'),
(43, '1', 'CSE 1105', '420', 3, '10/21/2017', '10-01-17', '2017-09-30 19:19:32', 'taken', 'tishpish'),
(44, '1', 'CSE 1105', '420', 3, '10/28/2017', '10-01-17', '2017-09-30 19:19:32', 'taken', 'tishpish'),
(45, '1', 'CSE 1103', '307', 0, '10/01/2017', '10-01-17', '2017-09-30 19:21:12', 'taken', 'fghcfgjcvm sdgxfg'),
(46, '1', 'CSE 1103', '307', 0, '10/08/2017', '10-01-17', '2017-09-30 19:21:12', 'taken', 'fghcfgjcvm sdgxfg'),
(47, '1', 'CSE 1103', '307', 0, '10/29/2017', '10-01-17', '2017-09-30 19:21:12', 'taken', 'fghcfgjcvm sdgxfg'),
(48, '1', 'CSE 1101', '320', 0, '10/02/2017', '10-01-17', '2017-09-30 19:27:24', 'taken', 'amni lagbe'),
(49, '1', 'CSE 1101', '320', 0, '10/09/2017', '10-01-17', '2017-09-30 19:27:24', 'taken', 'amni lagbe'),
(50, '1', 'CSE 1101', '320', 0, '10/16/2017', '10-01-17', '2017-09-30 19:27:24', 'taken', 'amni lagbe'),
(51, '1', 'CSE 1101', '320', 0, '10/23/2017', '10-01-17', '2017-09-30 19:27:24', 'taken', 'amni lagbe'),
(52, '1', 'CSE 1101', '420', 0, '10/10/2017', '10-01-17', '2017-10-01 05:02:30', 'taken', 'lol ken lagbe abar'),
(53, '1', 'CSE 1101', '420', 0, '10/17/2017', '10-01-17', '2017-10-01 05:02:30', 'taken', 'lol ken lagbe abar'),
(54, '1', 'CSE 1105', '420', 2, '10/01/2017', '10-01-17', '2017-10-01 06:58:44', 'taken', 'rhd dy yhfj  hjy '),
(55, '1', 'CSE 1105', '413', 2, '10/01/2017', '10-01-17', '2017-10-01 06:58:44', 'taken', 'rhd dy yhfj  hjy '),
(56, '1', 'CSE 1105', '307', 2, '10/01/2017', '10-01-17', '2017-10-01 06:58:44', 'taken', 'rhd dy yhfj  hjy '),
(57, '1', 'CSE 1105', '320', 2, '10/01/2017', '10-01-17', '2017-10-01 06:58:44', 'taken', 'rhd dy yhfj  hjy '),
(58, '1', 'CSE 1105', '420', 0, '10/02/2017', '10-01-17', '2017-10-01 06:59:06', 'taken', 'trytuty'),
(59, '1', 'CSE 1105', '420', 0, '10/09/2017', '10-01-17', '2017-10-01 06:59:06', 'taken', 'trytuty'),
(60, '1', 'CSE 1105', '420', 0, '10/16/2017', '10-01-17', '2017-10-01 06:59:06', 'taken', 'trytuty'),
(61, '1', 'CSE 1105', '420', 0, '10/23/2017', '10-01-17', '2017-10-01 06:59:06', 'taken', 'trytuty'),
(62, '1', 'CSE 1105', '420', 0, '10/30/2017', '10-01-17', '2017-10-01 06:59:06', 'taken', 'trytuty'),
(63, '1', 'CSE 1101', '420', 1, '10/01/2017', '10-01-17', '2017-10-01 08:25:40', 'taken', 'dassafsaa'),
(64, '1', 'CSE 1101', '413', 4, '10/01/2017', '10-01-17', '2017-10-01 08:25:47', 'taken', 'dassafsaaasdsafdas');

-- --------------------------------------------------------

--
-- Table structure for table `CourseList`
--

CREATE TABLE `CourseList` (
  `CourseNumber` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `CourseList`
--

INSERT INTO `CourseList` (`CourseNumber`) VALUES
('CSE 1101'),
('CSE 1102'),
('CSE 1103'),
('CSE 1104'),
('CSE 1105'),
('Others');

-- --------------------------------------------------------

--
-- Table structure for table `LabBookingList`
--

CREATE TABLE `LabBookingList` (
  `BookingID` bigint(20) NOT NULL,
  `UserID` varchar(2000) NOT NULL,
  `CourseNumber` varchar(100) NOT NULL,
  `RoomNumber` varchar(100) NOT NULL,
  `Position` int(11) NOT NULL,
  `RequestedDate` varchar(100) NOT NULL,
  `Date` varchar(100) NOT NULL,
  `Time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `Status` varchar(100) NOT NULL,
  `Comment` varchar(2000) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `LabBookingList`
--

INSERT INTO `LabBookingList` (`BookingID`, `UserID`, `CourseNumber`, `RoomNumber`, `Position`, `RequestedDate`, `Date`, `Time`, `Status`, `Comment`) VALUES
(1, '1', 'CSE 1101', '307', 0, '10/01/2017', '10-01-17', '2017-10-01 08:36:00', 'taken', 'contest'),
(2, '1', 'CSE 1104', '427', 4, '10/01/2017', '10-01-17', '2017-10-01 08:36:06', 'taken', 'contest'),
(3, '1', 'CSE 1104', '405', 4, '10/01/2017', '10-01-17', '2017-10-01 08:36:17', 'taken', 'contest'),
(4, '1', 'CSE 1101', '427', 0, '10/01/2017', '10-01-17', '2017-10-01 08:44:20', 'taken', 'hello world');

-- --------------------------------------------------------

--
-- Table structure for table `LabList`
--

CREATE TABLE `LabList` (
  `LabNumber` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `LabList`
--

INSERT INTO `LabList` (`LabNumber`) VALUES
('307'),
('313'),
('405'),
('427');

-- --------------------------------------------------------

--
-- Table structure for table `RoomList`
--

CREATE TABLE `RoomList` (
  `RoomNumber` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `RoomList`
--

INSERT INTO `RoomList` (`RoomNumber`) VALUES
('420'),
('413'),
('307'),
('320');

-- --------------------------------------------------------

--
-- Table structure for table `TimeSlot`
--

CREATE TABLE `TimeSlot` (
  `Position` int(11) NOT NULL,
  `TimeRange` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `TimeSlot`
--

INSERT INTO `TimeSlot` (`Position`, `TimeRange`) VALUES
(0, '08:30am - 10:00am'),
(1, '10:00am - 11:30am'),
(2, '11:30am - 01:00pm'),
(3, '02:00pm - 03:30pm'),
(4, '03:30pm - 05:00pm');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ClassBookingList`
--
ALTER TABLE `ClassBookingList`
  ADD PRIMARY KEY (`BookingID`);

--
-- Indexes for table `CourseList`
--
ALTER TABLE `CourseList`
  ADD PRIMARY KEY (`CourseNumber`);

--
-- Indexes for table `LabBookingList`
--
ALTER TABLE `LabBookingList`
  ADD PRIMARY KEY (`BookingID`);

--
-- Indexes for table `TimeSlot`
--
ALTER TABLE `TimeSlot`
  ADD PRIMARY KEY (`Position`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `ClassBookingList`
--
ALTER TABLE `ClassBookingList`
  MODIFY `BookingID` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=65;
--
-- AUTO_INCREMENT for table `LabBookingList`
--
ALTER TABLE `LabBookingList`
  MODIFY `BookingID` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
